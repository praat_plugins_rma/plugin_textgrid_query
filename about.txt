Plugin: TextGrid Query
Author: Rolando Muñoz A. <rmunoz@gmail.com>
Homepage: https://gitlab.com/praat_plugins_rma/plugin_textgrid_query
Version: 0.0.1
Last modified: 30-03-2018
Description:
  - Search contents in TextGrids and run some actions on them.
Depends:
  - Praat: 6.32.0+
License: GPL3
How to cite: 
 - Muñoz A., Rolando (2018). TextGrid Query[Praat plug-in]. Version 0.0.1, retrived 03 March 2018 from https://gitlab.com/praat_plugins_rma/plugin_indexer