# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.0.0] - 2017-08-20
### Added
  - `Get word list from...` in the fixed menu (`Praat > Goodies > TextGrid query`)
  - `To Table (word list)...` in the dynamic menu
  
### Changed
  - Create a table object
  - You can optionally remove the duplicates from that table
  - You can optionally print the table

### Remove
  - `Get unique interval list` in the dynamic menu
  - `Get unique linked interval list` in the dynamic menu

## [1.0.0] - 2017-08-04
### Added
  - `Get unique interval list`   
List all intervals of a single tier without duplicated cases.

  - `Get unique linked interval list`   
List all the intervals of a tier and their parent intervals from multiple tiers without duplicated cases
