# TextGrid Query

It is a Praat plug-in which provides a set of useful commands to summarize annotations.
 
After you have installed the plug-in, you can use it inmediately. To do it, select a TextGrid in the Object window. Then, go to `Query` in the dynamic menu. The followings buttons should now be available:

- `To Table (word list)`   
List all the intervals of a tier and their parent intervals from multiple tiers.

In the fixed menu, go to `Praat > Goodies >  TextGrid query`. You will find these commands:

- `Get word list from...`  
Collect all the words from different TextGrids stored under the same directory.
