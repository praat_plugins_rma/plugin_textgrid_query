# Copyright 2017 Rolando Muñoz Aramburú

if praatVersion < 6033
  appendInfoLine: "Plug-in name: TextGrid Query"
  appendInfoLine: "Warning: This plug-in only works on Praat version above 6.0.32. Please, get a more recent version of Praat."
  appendInfoLine: "Praat website: http://www.fon.hum.uva.nl/praat/"
endif

Add action command: "TextGrid", 1, "", 0, "", 0, "Word list...", "Tabulate -", 2, "scripts/get_parent_intervals.praat"

## Static menu
Add menu command: "Objects", "Goodies", "TextGrid query", "", 0, ""
Add menu command: "Objects", "Goodies", "Get word list from...", "TextGrid query", 1, "scripts/get_parent_intervals_do_all.praat"
Add menu command: "Objects", "Goodies", "-", "TextGrid query", 1, ""

Add menu command: "Objects", "Goodies", "About", "TextGrid query", 1, "scripts/about.praat"
