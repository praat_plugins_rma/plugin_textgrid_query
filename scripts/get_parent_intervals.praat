# Copyright 2017 Rolando Muñoz Aramburú

include ../procedures/config.proc
include ../procedures/link_intervals.proc

tg = selected("TextGrid")
@config.init: "../preferences.txt"
selectObject: tg

beginPause: "link intervals from a child tier to their parent tiers"
  word: "Child tier", config.init.return$["child_tier"]
  sentence: "Parent tiers", config.init.return$["parent_tier"]
  boolean: "Remove duplicates", number(config.init.return$["get_parent_intervals.remove_duplicates"])
  boolean: "Print", number(config.init.return$["get_parent_intervals.print"])
clicked = endPause: "Continue", "Quit", 1

if clicked = 2
  exitScript()
endif

@config.setField: "child_tier", child_tier$
@config.setField: "parent_tier", parent_tiers$

# Get table from TextGrid
selectObject: tg
tb = nowarn Down to Table: "no", 16, "yes", "no"

# Get tree structure
@linkIntervals: child_tier$, parent_tiers$
tb_tree = selected("Table")
Rename: "word list"

if remove_duplicates
  child_tier$ = replace_regex$(child_tier$, ".+", "&.text", 0)
  parent_tiers$ = replace_regex$(parent_tiers$, ".+", "&.text", 0)

  # Get unique values from tiers child and parent
  tb_tree_unique = Collapse rows: child_tier$ + " " + parent_tiers$ , "", "", "", "", ""
  Rename: "word list"
  removeObject: tb_tree
  tb_tree = tb_tree_unique
else
  # Remove *.tmax and *.tmin columns
  ncol = Object_'tb_tree'.ncol
  for i to ncol
    column_name$= Get column label: i
    if index_regex(column_name$, ".+(tmin|tmax)$")
      Remove column: column_name$
      i -= 1
      ncol -= 1
    endif
  endfor
endif

if print
  List: "yes"
endif

removeObject: tb