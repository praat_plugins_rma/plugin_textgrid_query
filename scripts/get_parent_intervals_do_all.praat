# Copyright 2017 Rolando Muñoz Aramburú

include ../procedures/config.proc
include ../procedures/link_intervals.proc

@config.init: "../preferences.txt"

beginPause: "link intervals from a child tier to their parent tiers"
  word: "TextGrid folder", config.init.return$["textgrid_dir"]
  word: "Child tier", config.init.return$["child_tier"]
  sentence: "Parent tiers", config.init.return$["parent_tier"]
  boolean: "Remove duplicates", number(config.init.return$["get_parent_intervals.remove_duplicates"])
  boolean: "Print", number(config.init.return$["get_parent_intervals.print"])
clicked = endPause: "Continue", "Quit", 1

if clicked = 2
  exitScript()
endif

@config.setField: "textgrid_dir", textGrid_folder$
@config.setField: "child_tier", child_tier$
@config.setField: "parent_tier", parent_tiers$
@config.setField: "get_parent_intervals.remove_duplicates", string$(remove_duplicates)
@config.setField: "get_parent_intervals.print", string$(print)

str = Create Strings as file list: "fileList", textGrid_folder$ + "/*.TextGrid"
nFiles = Get number of strings

for i to nFiles
  tg_name$= object$[str, i]
  tg = Read from file: textGrid_folder$ + "/" + tg_name$

  # Get table from TextGrid
  tb = nowarn Down to Table: "no", 16, "yes", "no"

  # Get tree structure
  @linkIntervals: child_tier$, parent_tiers$
  tb_tree[i] = selected("Table")

  removeObject: tg, tb
endfor

removeObject: str

for i to nFiles
  if i = 1
    selectObject: tb_tree[i]
  else
    plusObject: tb_tree[i]
  endif
endfor

tb_tree= Append
Rename: "word list"

# Remove all tb_objects objects
for i to nFiles
  removeObject: tb_tree[i]
endfor

if remove_duplicates
  child_tier$ = replace_regex$(child_tier$, ".+", "&.text", 0)
  parent_tiers$ = replace_regex$(parent_tiers$, ".+", "&.text", 0)

  # Get unique values from tiers child and parent
  tb_tree_unique = Collapse rows: child_tier$ + " " + parent_tiers$ , "", "", "", "", ""
  Rename: "word list"
  removeObject: tb_tree
  tb_tree = tb_tree_unique
else
  # Remove *.tmax and *.tmin columns
  ncol = Object_'tb_tree'.ncol
  for i to ncol
    column_name$= Get column label: i
    if index_regex(column_name$, ".+(tmin|tmax)$")
      Remove column: column_name$
      i -= 1
      ncol -= 1
    endif
  endfor
endif

selectObject: tb_tree
if print
  List: "yes"
endif
pauseScript: "Completed successfully"
